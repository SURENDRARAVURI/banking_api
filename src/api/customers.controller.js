import bcrypt from "bcryptjs"
import jwt from "jsonwebtoken"
import CustomersDAO from "../dao/customersDAO"
const ObjectID = require('mongodb').ObjectID

export class Customers {
  constructor( { name, email, mobileno, address, accounts} = {}) {
    this.name = name
    this.email = email
    this.mobileno = mobileno
    this.address = address
    this.accounts=accounts
  }
  toJson() {
    return { name: this.name, email: this.email, mobileno: this.mobileno, address: this.address, accounts:this.accounts}
  }
}

export default class CustomersController {

  static async create(req, res) {
    try {
      const customerFromBody = req.body
      let errors = {}
      
      const customerFromDB = await CustomersDAO.getCustomer(customerFromBody.email) 
      if(customerFromDB && customerFromDB.email === customerFromBody.email){
        errors.email = "Customer with email already exists"
      }     
      if (customerFromBody && customerFromBody.mobileno.length < 10) {
        errors.mobileno = "Mobile No must be at least 10 characters."
      }
      if (customerFromBody && customerFromBody.name.length < 3) {
        errors.name = "You must specify a name of at least 3 characters."
      }

      if (Object.keys(errors).length > 0) {
        res.status(400).json(errors)
        return
      }

      const customerInfo = {
        ...customerFromBody,
      }

      const insertResult = await CustomersDAO.createCustomer(customerInfo)
      if (!insertResult.success) {
        errors.email = insertResult.error
      }
      

      if (Object.keys(errors).length > 0) {
        res.status(400).json(errors)
        return
      }

      const customers = new Customers(customerFromBody)

      res.json({
        info: customers,
      })
    } catch (e) {
      console.error(e)
      res.json(e)
    }
  }

  static async addBankAccount(req, res) {
    try {
      const accountBody = req.body
      let errors = {}
      const accountFromDB = await CustomersDAO.getAccount(accountBody) 
      
      console.log("accountFromDB: "+JSON.stringify(accountFromDB))
      if(accountFromDB && accountFromDB.accounts.length > 0 ){
        errors.accountno = "AccountNO already exists"
      } 

      if (Object.keys(errors).length > 0) {
        res.status(400).json(errors)
        return
      }

      const accountInfo = {
        ...accountBody,
      }

      const insertResult = await CustomersDAO.addAccount(accountInfo)
      if (!insertResult.success) {
        errors.email = insertResult.error
      }
      
      const customerFromDB = await CustomersDAO.getCustomer(accountBody.email) 
      

      if (Object.keys(errors).length > 0) {
        res.status(400).json(errors)
        return
      }

      const customers = new Customers(customerFromDB)

      res.json({
        info: customers.toJson(),
      })
    } catch (e) {
      res.status(500).json({ error: e })
    }
  }

  static async getAccountBalance(req, res) {
    try {
      const accountBody = req.body
      let errors = {}
      const accountFromDB = await CustomersDAO.getAccount(accountBody) 
      
      console.log("accountFromDB: "+JSON.stringify(accountFromDB))
      if(accountFromDB && accountFromDB.accounts.length === 0 ){
        errors.accountno = "No AccountNO exists"
        
      } 

      if (Object.keys(errors).length > 0) {
        res.status(400).json(errors)
        return
      }
      const accountBalance = accountFromDB.accounts[0]
      console.log(accountBalance)
      res.json({
        info: accountBalance,
      })
    }
    catch (e) {
      res.status(500).json({ error: e })
    }
  }
}
