import bcrypt from "bcryptjs"
import shortid from "shortid"
import TransactionsDAO from "../dao/transactionsDAO"
const circularJSON = require('circular-json');
const ID = require("nodejs-unique-numeric-id-generator")

export class Transactions {
  constructor( { transactionid, fromaccount, toaccount, amount, type, remarks, date} = {}) {
    this.transactionid = transactionid
    this.fromaccount = fromaccount
    this.toaccount = toaccount
    this.amount = amount
    this.type = type
    this.remarks = remarks
    this.date = date
  }
  toJson() {
    return { 
      transactionid: this.transactionid, 
      fromaccount: this.fromaccount, 
      toaccount: this.toaccount, 
      amount: this.amount, 
      type: this.type,
      remarks: this.remarks,
      date: this.date,
    }
  }
}

export default class TransactionsController {

  static async create(req, res) {
    try {
      const transactionBody = req.body      
      console.log("TransactionBody: "+JSON.stringify(transactionBody))
      transactionBody["transactionid"] = shortid.generate() //ID.generate(new Date().toJSON())+shortid.generate()
      transactionBody["date"] = new Date()
      let errors = {}

      const transactionInfo = {
        ...transactionBody,
      }
      const accountBody = {accountno:transactionBody.fromaccount}
      const balanceDetails  = await TransactionsDAO.getAccountBalance(accountBody)
      if(balanceDetails && balanceDetails.amount < transactionBody.amount){
        errors.amount = "Insufficient Funds in Account"
      }

      if (Object.keys(errors).length > 0) {
        res.status(400).json(errors)
        return
      }
      const insertResult = await TransactionsDAO.createTransaction(transactionBody)
      if (!insertResult.success) {
        errors.transactionid = insertResult.error
      }      

      if (Object.keys(errors).length > 0) {
        res.status(400).json(errors)
        return
      }      

      const transactions = new Transactions(transactionBody)

      res.json({
        info: transactions,
      })
    } catch (e) {
      console.error(e)
      res.json(e)
    }
  }

  static async getAccountBalance(req, res){
    try {
      const accountBody = req.body
      const balance = await TransactionsDAO.getAccountBalance(accountBody)
      console.log("balance: "+JSON.stringify(balance))
      // const credits = await TransactionsDAO.getCreditTotal(accountBody)
      // console.log("credits: "+JSON>stringify(credits))
      // const debits = await TransactionsDAO.getDebitTotal(accountBody)      
      // console.log("debits: "+JSON>stringify(debits))
      res.json({
        info: balance,
      })
    } catch (e) {
      console.error(e)
      res.json(e)
    }
  }

  static async getTransactions(req, res){
    try {
      const accountBody = req.body
      const transactionHIstory = await TransactionsDAO.getTransactions(accountBody)
      console.log("TransactionHistory: "+circularJSON.stringify(transactionHIstory))
      res.json({
        info: transactionHIstory,
      })
    } catch (e) {
      console.error(e)
      res.json(e)
    }
  }

}
