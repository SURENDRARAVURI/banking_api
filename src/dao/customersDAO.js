let customers

export default class CustomersDAO {
  static async injectDB(conn) {
    if (customers) {
      return
    }
    try {
      //console.log("conn.db: "+process.env.BANKING_DB)
      customers = await conn.db(process.env.BANKING_DB).collection("customers")
    } catch (e) {
      console.error(`Unable to establish collection handles in customersDAO: ${e}`)
    }
  }

  static async getCustomer(email) {
    return await customers.findOne({ email: email })
  }

  static async createCustomer(customerInfo) {
    try {
      console.log("CreateCustomer: " + JSON.stringify(customerInfo))
      await customers.insertOne({
        name: customerInfo.name,
        email: customerInfo.email,
        mobileno: customerInfo.mobileno,
        address: customerInfo.address,
        accounts: customerInfo.accounts
      }, { w: "majority" })
      return { success: true }
    } catch (e) {
      if (String(e).startsWith("MongoError: E11000 duplicate key error")) {
        return { error: "A customer with the given email already exists." }
      }
      console.error(`Error occurred while adding new customer, ${e}.`)
      return { error: e }
    }
  }

  static async getAccount(accountInfo) {

    try {
      console.log("EMAIL: " + accountInfo.email + ", AccountNO: " + accountInfo.accountno)
      return await customers.findOne({
        'accounts': {
          '$elemMatch': {
            'accountno': accountInfo.accountno
          }
        }
      }, { "accounts.$": 1, name: 1 })
      
    } catch (e) {
      console.error(`Error occurred while getting account details, ${e}.`)
      return { error: e }
    }
  }

  
  static async addAccount(accountInfo) {
    const email = accountInfo.email
    const account = {
      accountno: accountInfo.accountno
    }
    console.log("EMAIL: " + email + ", AccountNO: " + account.accountno)
    try {
      await customers.updateOne({ email: email }, { "$push": { "accounts": account } }, { "upsert": true })
      return { success: true }
    } catch (e) {
      if (String(e).startsWith("MongoError: E11000 duplicate key error")) {
        return { error: "A Account with the given accoungt no already exists." }
      }
      console.error(`Error occurred while adding Bank Account, ${e}.`)
      return { error: e }
    }
  }
}
