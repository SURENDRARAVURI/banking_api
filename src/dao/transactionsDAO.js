
const circularJSON = require('circular-json');
let transactions

export default class TransactionsDAO {
  static async injectDB(conn) {
    if (transactions) {
      return
    }
    try {
      console.log("conn.db: "+process.env.BANKING_DB)
      transactions = await conn.db(process.env.BANKING_DB).collection("transactions")
      
    } catch (e) {
      console.error(`Unable to establish collection handles in transactionsDAO: ${e}`)
    }
  }  

  static async createTransaction(transactionInfo) {
    try {
      console.log("CreateTransaction: " + JSON.stringify(transactionInfo))
      if(transactionInfo.remarks === "INITIAL DEPOSIT"){
        await transactions.insertOne({
          transactionid : transactionInfo.transactionid,
          fromaccount : transactionInfo.toaccount,
          toaccount : null,
          amount : transactionInfo.amount,
          type : "Credit",
          remarks: transactionInfo.remarks,
          date: transactionInfo.date,
        }, { w: "majority" })
      }else{
        await transactions.insertOne({
          transactionid : transactionInfo.transactionid,
          fromaccount : transactionInfo.fromaccount,
          toaccount : transactionInfo.toaccount,
          amount : transactionInfo.amount,
          type : "Debit",
          remarks: transactionInfo.remarks,
          date: transactionInfo.date,
        }, { w: "majority" })
        await transactions.insertOne({
          transactionid : transactionInfo.transactionid,
          fromaccount : transactionInfo.toaccount,
          toaccount : transactionInfo.fromaccount,
          amount : transactionInfo.amount,
          type : "Credit",
          remarks: transactionInfo.remarks,
          date: transactionInfo.date,
        }, { w: "majority" })
      }
      
      return { success: true }
    } catch (e) {
      if (String(e).startsWith("MongoError: E11000 duplicate key error")) {
        return { error: "A TRansaction with the given Id already exists." }
      }
      console.error(`Error occurred while adding new Transaction, ${e}.`)
      return { error: e }
    }
  }

  static async getCreditTotal(accountBody) {
    try {
      console.log("getCreditTotal: " + JSON.stringify(accountBody))
      const creditPipeline = [
        {
          '$match': {
            'fromaccount': accountBody.accountno,
            'type': 'Credit'
          }
        }, {
          '$project': {
            'fromaccount': 1,
            'amount': 1
          }
        }, {
          '$group': {
            '_id': {
              'account': '$fromaccount',
              'type': 'Credit'
            },
            'TotalSum': {
              '$sum': '$amount'
            }
          }
        }
      ]
      console.log("PIPELINE: "+JSON.stringify(creditPipeline))
      return await transactions.aggregate(creditPipeline).next()
    } catch (e) {
      console.error(`Error occurred while adding new Transaction, ${e}.`)
      return { error: e }
    }
  }

  static async getDebitTotal(accountBody) {
    try {
      console.log("getDebitTotal: " + JSON.stringify(accountBody))
      const debitPipeline = [
        {
          '$match': {
            'fromaccount': accountBody.accountno,
            'type': 'Debit'
          }
        }, {
          '$project': {
            'fromaccount': 1,
            'amount': 1
          }
        }, {
          '$group': {
            '_id': {
              'account': '$fromaccount',
              'type': 'Debit'
            },
            'TotalSum': {
              '$sum': '$amount'
            }
          }
        }
      ]
      return await transactions.aggregate(debitPipeline).next()
    } catch (e) {
      console.error(`Error occurred while adding new Transaction, ${e}.`)
      return { error: e }
    }
  }
  
  static async getAccountBalance(accountBody) {
    try {
      console.log("getAccountBalance: " + JSON.stringify(accountBody))
      const creditPipeline = [
        {
          '$match': {
            'fromaccount': accountBody.accountno,
            'type': 'Credit'
          }
        }, {
          '$project': {
            'fromaccount': 1,
            'amount': 1
          }
        }, {
          '$group': {
            '_id': {
              'account': '$fromaccount',
              'type': 'Credit'
            },
            'TotalSum': {
              '$sum': '$amount'
            }
          }
        }
      ]

      const debitPipeline = [
        {
          '$match': {
            'fromaccount': accountBody.accountno,
            'type': 'Debit'
          }
        }, {
          '$project': {
            'fromaccount': 1,
            'amount': 1
          }
        }, {
          '$group': {
            '_id': {
              'account': '$fromaccount',
              'type': 'Debit'
            },
            'TotalSum': {
              '$sum': '$amount'
            }
          }
        }
      ]
      const credits =  await transactions.aggregate(creditPipeline).next()
      console.log("CREDITS: "+JSON.stringify(credits))
      const debits =  await transactions.aggregate(debitPipeline).next()
      console.log("DEBITS: "+JSON.stringify(debits))
      const balance = {
        accountno:accountBody.accountno,
        creditTotal:credits?credits.TotalSum:0,
        debitTotal:debits?debits.TotalSum:0,
        balance: (credits?credits.TotalSum:0) - (debits?debits.TotalSum:0)
      }
      return balance
    } catch (e) {
      console.error(`Error occurred while Retrieving balances, ${e}.`)
      return { error: e }
    }
  }

  static async getTransactions(accountBody) {
    try {
      console.log("getTransactions: " + JSON.stringify(accountBody))
      const pipeline = [
        {
            '$match': {
                'fromaccount': accountBody.accountno
            }
        }, {
            '$project': {
                'fromaccount': 1, 
                'amount': 1, 
                'type': 1
            }
        }
    ]
    return  await transactions.aggregate(pipeline).next()
    } catch (e) {
      console.error(`Error occurred while Retrieving Transactions, ${e}.`)
      return { error: e }
    }
  }
}
