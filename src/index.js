import app from "./server"
import { MongoClient } from "mongodb"
import UsersDAO from "./dao/usersDAO"
import CustomersDAO from "./dao/customersDAO"
import TransactionsDAO from "./dao/transactionsDAO"

const port = process.env.PORT || 8000

// console.log("process.env.BANKING_DB_URI: "+process.env.BANKING_DB_URI);
// console.log("process.env.PORT: "+process.env.PORT);
MongoClient.connect(
  process.env.BANKING_DB_URI,
  { 
    poolSize:50,
    writeConcern: {
      w: "majority",
      j: true,
      wtimeout: 2500,
    },
    useNewUrlParser: true,
    useUnifiedTopology:true 
  },
)
  .catch(err => {
    console.error(err.stack)
    process.exit(1)
  })
  .then(async client => {      
    await CustomersDAO.injectDB(client)
    await UsersDAO.injectDB(client)
    await TransactionsDAO.injectDB(client)
    app.listen(port, () => {
      console.log(`listening on port ${port}`)
    })
  })
