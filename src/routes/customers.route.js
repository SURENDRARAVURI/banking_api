import { Router } from "express"
import customersCtrl from "../api/customers.controller"

const router = new Router()

// associate put, delete, and get(id)
router.route("/create").post(customersCtrl.create)
router.route("/addAccount").post(customersCtrl.addBankAccount)
//router.route("/accountBalance").get(customersCtrl.getAccountBalance)

export default router
