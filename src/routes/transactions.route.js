import { Router } from "express"
import transactionsCtrl from "../api/transactions.controller"

const router = new Router()

// associate put, delete, and get(id)
router.route("/create").post(transactionsCtrl.create)
router.route("/getAccountBalance").post(transactionsCtrl.getAccountBalance)
router.route("/getTransactions").post(transactionsCtrl.getTransactions)

export default router
