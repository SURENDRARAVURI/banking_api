const MongoClient = require("mongodb").MongoClient
const NodeEnvironment = require("jest-environment-node")

module.exports = class MongoEnvironment extends NodeEnvironment {
  async setup() {
    if (!this.global.bankClient) {
      this.global.bankClient = await MongoClient.connect(
        process.env.BANKING_DB_URI,
        {
          poolSize: 50,
          writeConcern: {
            w: "majority",
            j: true,
            wtimeout: 2500,
          },
          useNewUrlParser: true,
          useUnifiedTopology: true
        }
      )
      await super.setup()
    }
  }

  async teardown() {
    await this.global.bankClient.close()
    await super.teardown()
  }

  runScript(script) {
    return super.runScript(script)
  }
}
