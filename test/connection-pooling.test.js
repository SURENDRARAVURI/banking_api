import UsersDAO from "../src/dao/usersDAO"

describe("Connection Pooling", () => {
  beforeAll(async () => {
    await UsersDAO.injectDB(global.bankClient)
  })

  test("Connection pool size is 50", async () => {
    const response = await UsersDAO.getConfiguration()
    expect(response.poolSize).toBe(50)
  })
})
