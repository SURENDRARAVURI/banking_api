import UsersDAO from "../src/dao/usersDAO"

describe("Connection", () => {
  beforeAll(async () => {
    await UsersDAO.injectDB(global.bankClient)
  })

  test("Can access MFlix data", async () => {
    const bank = global.bankClient.db(process.env.BANKING_DB)
    const collections = await bank.listCollections().toArray()
    const collectionNames = collections.map(elem => elem.name)
    expect(collectionNames).toContain("users")
    expect(collectionNames).toContain("customers")
    expect(collectionNames).toContain("users")
  })

  test("Can retrieve a user by email", async () => {
    const email = "surendraaravuri@gmail.com"
    const user = await UsersDAO.getUser(email)
    expect(user.name).toEqual("surendra")
  })
})
